# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# gummi <gudmundure@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-15 02:12+0000\n"
"PO-Revision-Date: 2023-03-16 20:29+0000\n"
"Last-Translator: gummi <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Guðmundur Erlingsson"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "gudmundure@gmail.com"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Frídagar"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Viðburðir"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Verkefnalisti"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Annað"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 viðburður"
msgstr[1] "%1 viðburðir"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "No events"
msgstr "Engir viðburðir"

#: calendar/qml/MonthViewHeader.qml:67
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 %2"

#: calendar/qml/MonthViewHeader.qml:113
#, kde-format
msgid "Days"
msgstr "Dagar"

#: calendar/qml/MonthViewHeader.qml:119
#, kde-format
msgid "Months"
msgstr "Mánuðir"

#: calendar/qml/MonthViewHeader.qml:125
#, kde-format
msgid "Years"
msgstr "Ár"

#: calendar/qml/MonthViewHeader.qml:163
#, kde-format
msgid "Previous Month"
msgstr "Fyrri mánuður"

#: calendar/qml/MonthViewHeader.qml:165
#, kde-format
msgid "Previous Year"
msgstr "Fyrra ár"

#: calendar/qml/MonthViewHeader.qml:167
#, kde-format
msgid "Previous Decade"
msgstr "Fyrri áratugur"

#: calendar/qml/MonthViewHeader.qml:184
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Í dag"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgid "Reset calendar to today"
msgstr "Endurstilla dagatal á daginn í dag"

#: calendar/qml/MonthViewHeader.qml:196
#, kde-format
msgid "Next Month"
msgstr "Næsti mánuður"

#: calendar/qml/MonthViewHeader.qml:198
#, kde-format
msgid "Next Year"
msgstr "Næsta ár"

#: calendar/qml/MonthViewHeader.qml:200
#, kde-format
msgid "Next Decade"
msgstr "Næsti áratugur"

#: calendar/qml/MonthViewHeader.qml:256
#, kde-format
msgid "Keep Open"
msgstr "Halda opnu"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Grunnstilla…"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Skjálæsing virk"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Stillir hvort skjárinn verði læstur eftir tiltekinn tíma."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Tímamörk skjáhvílu"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Stillir hversu margar mínútur líða þar til skjárinn er læstur."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Ný seta"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Síur"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:41
#, kde-format
msgid "Select Plasmoid File"
msgstr "Velja plasmíðskrá"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:65
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Uppsetning á pakkanum %1 mistókst."

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:65
#, kde-format
msgid "Installation Failure"
msgstr "Uppsetning mistókst"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:111
#, kde-format
msgid "All Widgets"
msgstr "Allar græjur"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
#, kde-format
msgid "Running"
msgstr "Í keyrslu"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Hægt að fjarlægja"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
#, kde-format
msgid "Categories:"
msgstr "Flokkar:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:198
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Sækja nýjar Plasma-græjur"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:207
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Setja upp græju úr staðbundinni skrá..."
