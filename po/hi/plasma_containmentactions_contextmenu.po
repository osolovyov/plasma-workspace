# translation of plasma_containmentactions_contextmenu.po to Hindi
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Rajesh Ranjan <rajesh672@gmail.com>, 2010.
# Raghavendra Kamath <raghu@raghukamath.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_containmentactions_contextmenu\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-17 03:28+0000\n"
"PO-Revision-Date: 2021-07-01 18:09+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: kde-hindi\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.2\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"

#: menu.cpp:98
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "केरनर दिखाएँ"

#: menu.cpp:103
#, kde-format
msgid "Open Terminal"
msgstr ""

#: menu.cpp:107
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "स्क्रीन पर ताला लगाएँ"

#: menu.cpp:116
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Leave…"
msgstr "छोड़ें..."

#: menu.cpp:125
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Display Settings…"
msgstr ""

#: menu.cpp:290
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "संदर्भ मेन्यू प्लगइन विन्यस्त करें"

#: menu.cpp:300
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "[अन्य क्रियाएं]"

#: menu.cpp:303
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "वालपेपर क्रियाएं"

#: menu.cpp:307
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "[विभाजक]"

#, fuzzy
#~| msgid "Run Command..."
#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Run Command..."
#~ msgstr "कमांड चलाएँ..."
