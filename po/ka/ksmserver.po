# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-15 02:12+0000\n"
"PO-Revision-Date: 2023-03-17 05:27+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "გასვლა გაუქმდა %1-ის მიერ"

#: main.cpp:77
#, kde-format
msgid "$HOME not set!"
msgstr "$HOME დაყენებული არაა!"

#: main.cpp:81 main.cpp:89
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr "$HOME საქაღალდე (%1) არ არსებობს."

#: main.cpp:84
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""
"$HOME საქაღალდეში (%1) ჩაწერის უფლებების გარეშე. თუ ეს ძალით ქენით, თქვენს "
"გარემოში დააყენეთ ცვლადი <envar>kDE_HOME_READONLY=1</envar>."

#: main.cpp:91
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr "$HOME საქაღალდიდან (%1) წაკითხვის უფლების გარეშე."

#: main.cpp:95
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr "$HOME საქაღალდეში (%1) თავისუფალი ადგილი აღარაა."

#: main.cpp:98
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr "$HOME საქაღალდეში (%2) ჩაწერა %1 შეცდომით დასრულდა"

#: main.cpp:111 main.cpp:146
#, kde-format
msgid "No write access to '%1'."
msgstr "ჩაწერის წვდომის გარეშე: '%1'."

#: main.cpp:113 main.cpp:148
#, kde-format
msgid "No read access to '%1'."
msgstr "წაკითხვის წვდომის გარეშე: '%1'."

#: main.cpp:121 main.cpp:134
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr "დროებით საქაღალდეში (%1) თავისუფალი ადგილი აღარაა."

#: main.cpp:124 main.cpp:137
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""
"დროებით საქაღალდეში (%2) ჩაწერა შემდეგი\n"
"   შეცდომით დასრულდა: %1"

#: main.cpp:152
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""
"Plasma-ის გაშვებისას ნაპოვნია\n"
"დაყენების შემდეგი პრობლემა:"

#: main.cpp:155
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""
"\n"
"\n"
"Plasma-ის გაშვების შეცდომა.\n"

#: main.cpp:162
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr "Plasma -ის დაყენების პრობლემა გაქვთ!"

#: main.cpp:196
#, kde-format
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"საიმედო Plasma-ის სესიების მმართველი, რომელიც სტანდარტულ X11R6-ის\n"
"სესიის მართვის პროტოკოლით (XSMP) ლაპარაკობს."

#: main.cpp:200
#, kde-format
msgid "Restores the saved user session if available"
msgstr "შენახული მომხმარებლის სესიის აღდგენა, თუ ის ხელმისაწვდომია"

#: main.cpp:203
#, kde-format
msgid "Also allow remote connections"
msgstr "დაშორებული მიერთებების დაშვება"

#: main.cpp:206
#, kde-format
msgid "Starts the session in locked mode"
msgstr "სესიის დაბლოკილ რეჟიმში გაშვება"

#: main.cpp:210
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""
"ეკრანის დაბლოკვის მხარდაჭერის გარეშე გაშვება. საჭიროა მხოლოდ აშინ, თუ ეკრანს "
"სხვა კომპონენტი ბლოკავს."

#: server.cpp:884
#, kde-format
msgid "Session Management"
msgstr "სესიების მართვა"

#: server.cpp:889
#, kde-format
msgid "Log Out"
msgstr "გასვლა"

#: server.cpp:894
#, kde-format
msgid "Shut Down"
msgstr "გამორთვა"

#: server.cpp:899
#, kde-format
msgid "Reboot"
msgstr "გადატვირთვა"

#: server.cpp:905
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "გასვლა დადასტურების გარეშე"

#: server.cpp:910
#, kde-format
msgid "Shut Down Without Confirmation"
msgstr "გამორთვა დადასტურების გარეშე"

#: server.cpp:915
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "გადატვირთვა დადასტურების გარეშე"

#~ msgid "No write access to $HOME directory (%1)."
#~ msgstr "$HOME საქაღალდეში (%1) ჩაწერის უფლების გარეშე."
