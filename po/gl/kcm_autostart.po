# translation of kcm_autostart.po to galician
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the kcm_autostart package.
#
# mvillarino <mvillarino@users.sourceforge.net>, 2008, 2009.
# Miguel Branco <mgl.branco@gmail.com>, 2008.
# marce villarino <mvillarino@users.sourceforge.net>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2011, 2012.
# Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>, 2015, 2017.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-04 02:12+0000\n"
"PO-Revision-Date: 2019-01-01 15:32+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: autostartmodel.cpp:322
#, fuzzy, kde-format
#| msgid "\"%1\" is not an absolute path."
msgid "\"%1\" is not an absolute url."
msgstr "«%1» non é unha ruta absoluta."

#: autostartmodel.cpp:325
#, kde-format
msgid "\"%1\" does not exist."
msgstr "«%1» non existe."

#: autostartmodel.cpp:328
#, kde-format
msgid "\"%1\" is not a file."
msgstr "«%1» non é un ficheiro."

#: autostartmodel.cpp:331
#, kde-format
msgid "\"%1\" is not readable."
msgstr "«%1» non é lexíbel."

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr ""

#: package/contents/ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: package/contents/ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: package/contents/ui/main.qml:95
#, fuzzy, kde-format
#| msgid "&Properties..."
msgid "Properties"
msgstr "&Propiedades…"

#: package/contents/ui/main.qml:101
#, fuzzy, kde-format
#| msgid "&Remove"
msgid "Remove"
msgstr "&Retirar"

#: package/contents/ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr ""

#: package/contents/ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr ""

#: package/contents/ui/main.qml:118
#, fuzzy, kde-format
#| msgid "Pre-KDE startup"
msgid "Pre-startup Scripts"
msgstr "Antes do inicio de KDE"

#: package/contents/ui/main.qml:121
#, fuzzy, kde-format
#| msgid "Logout"
msgid "Logout Scripts"
msgstr "Saír"

#: package/contents/ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: package/contents/ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""

#: package/contents/ui/main.qml:145
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "Engadir un script…"

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: package/contents/ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr ""

#: package/contents/ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr ""

#: package/contents/ui/main.qml:203
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Login Script…"
msgstr "Engadir un script…"

#: package/contents/ui/main.qml:209
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Logout Script…"
msgstr "Engadir un script…"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marce Villarino"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mvillarino@users.sourceforge.net"

#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Módulo de control dos inicios automáticos de sesión"

#, fuzzy
#~| msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "© 2006–2010 O equipo do Xestor de inicios automáticos"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Mantenedor"

#, fuzzy
#~| msgid "Advanced..."
#~ msgid "Add..."
#~ msgstr "Avanzado…"

#~ msgid "Shell script path:"
#~ msgstr "Ruta ao script da shell:"

#~ msgid "Create as symlink"
#~ msgstr "Crear como ligazón simbólica"

#~ msgid "Autostart only in Plasma"
#~ msgstr "Iniciar automaticamente só en Plasma"

#~ msgid "Name"
#~ msgstr "Nome"

#~ msgid "Command"
#~ msgstr "Orde"

#~ msgid "Status"
#~ msgstr "Estado"

#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Executar ao"

#~ msgid "Session Autostart Manager"
#~ msgstr "Xestor de inicios automáticos de sesión"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Activado"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Desactivado"

#~ msgid "Desktop File"
#~ msgstr "Ficheiro de escritorio"

#~ msgid "Script File"
#~ msgstr "Ficheiro de script"

#~ msgid "Add Program..."
#~ msgstr "Engadir un programa…"

#~ msgid "Startup"
#~ msgstr "Inicio"

#~ msgid "Before session startup"
#~ msgstr "Antes do inicio da sesión"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "Só se permite configurar o ambiente con ficheiros con extensión «.sh»."

#~ msgid "Shutdown"
#~ msgstr "Apagar"
