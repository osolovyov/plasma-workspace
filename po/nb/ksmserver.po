# Translation of ksmserver to Norwegian Bokmål
#
# Hans Petter Bieker <bieker@kde.org>, 2000.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2002, 2003, 2008, 2009, 2010, 2012, 2014.
# Knut Yrvin <knut.yrvin@gmail.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-15 02:12+0000\n"
"PO-Revision-Date: 2014-08-11 19:30+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "Utlogging avbrutt av «%1»"

#: main.cpp:77
#, kde-format
msgid "$HOME not set!"
msgstr "$HOME ikke oppgitt."

#: main.cpp:81 main.cpp:89
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr "$HOME-mappa (%1) finnes ikke."

#: main.cpp:84
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:91
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr "Ingen lesetilgang til $HOME-mappa (%1),"

#: main.cpp:95
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr "$HOME-mappa (%1) har ikke mer diskplass."

#: main.cpp:98
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr "Skriving til $HOME-mappa (%2) mislyktes med feilen «%1»."

#: main.cpp:111 main.cpp:146
#, kde-format
msgid "No write access to '%1'."
msgstr "Ingen skrivetilgang til «%1»."

#: main.cpp:113 main.cpp:148
#, kde-format
msgid "No read access to '%1'."
msgstr "Ingen lesetilgang til «%1»."

#: main.cpp:121 main.cpp:134
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr "Den midlertidige mappa (%1) har ikke mer diskplass."

#: main.cpp:124 main.cpp:137
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""
"Skriving til  den midlertidige mappa (%2) mislyktes med\n"
"    feilen «%1»."

#: main.cpp:152
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:155
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:162
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr "Installasjonsproblem med Plasma Workspace."

#: main.cpp:196
#, kde-format
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""

#: main.cpp:200
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Gjenopprett forrige økt hvis tilgjengelig"

#: main.cpp:203
#, kde-format
msgid "Also allow remote connections"
msgstr "Godta også fjernforbindelser"

#: main.cpp:206
#, kde-format
msgid "Starts the session in locked mode"
msgstr "Starter økta som låst"

#: main.cpp:210
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:884
#, kde-format
msgid "Session Management"
msgstr ""

#: server.cpp:889
#, kde-format
msgid "Log Out"
msgstr "Logg ut"

#: server.cpp:894
#, kde-format
msgid "Shut Down"
msgstr ""

#: server.cpp:899
#, kde-format
msgid "Reboot"
msgstr ""

#: server.cpp:905
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "Logg ut uten bekreftelse"

#: server.cpp:910
#, fuzzy, kde-format
#| msgid "Halt Without Confirmation"
msgid "Shut Down Without Confirmation"
msgstr "Stopp datamaskinen uten bekreftelse"

#: server.cpp:915
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "Start datamaskinen om igjen uten bekreftelse"
